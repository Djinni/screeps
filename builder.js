/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('builder'); // -> 'a thing'
 */
 module.exports = function(creep) {

    //creep.memory.project = null;
    //creep.memory.action = undefined;
    

    if(creep.energy === 0) {
        creep.memory.action = "collecting"
        if (creep.memory.project) {
            var oldProject = Game.getObjectById(creep.memory.project);
            if (oldProject && Memory.structures[oldProject.id]) {
                Memory.structures[oldProject.id].builder = null;
            }
        }

        creep.memory.project = null;
    }
    if (creep.memory.action === "collecting" && creep.energy === creep.energyCapacity) {
        creep.memory.server = null;
        creep.memory.action = "building";
        get_project(creep);
    }

    if (creep.memory.action === undefined) {
        creep.memory.action = "building"
        get_project(creep);
    }

    if (creep.memory.action === "building" && creep.memory.project != null) {

        var project = Game.getObjectById(creep.memory.project);

        if (project != null) {

            //if project cannot be reached, give up
            if (creep.pos.getRangeTo(project) > 1) {
                if (creep.moveTo(project) == -2) {
                    if (Memory.structures[project.id]) {
                        Memory.structures[project.id].builder = null;
                    }
                    creep.memory.project = null;
                    creep.memory.action = undefined;
                }
            }
            
            //if project is a structure, repair it.
            if (project instanceof Structure && project.type != STRUCTURE_CONTROLLER) {
                creep.repair(project);
                if (project.hits > project.hitsMax *.8) {
                    if (Memory.structures[project.id]) {
                        Memory.structures[project.id].builder = null;
                    }
                    creep.memory.project = null;
                    creep.memory.action = undefined;

                }
            }

            //if project is a room controller, upgrade it
            if (project.id == creep.room.controller.id) {
                creep.moveTo(project);
                creep.upgradeController(project);
            }

            //if project is a Construction site, build it
            if (project instanceof ConstructionSite) {
                creep.build(project);
                if (project.progress == project.progressTotal) {
                    Memory.structures[project.id].builder = null;
                    creep.memory.project = null;
                    creep.memory.action = undefined;
                    
                }
            }
        } else {
            creep.memory.project = null;
            creep.memory.action = undefined;
        }
    }

    if (creep.memory.action === "collecting") {
        var spawn = creep.pos.findClosest(FIND_MY_SPAWNS);
        if (spawn) {
            creep.moveTo(spawn);
            spawn.transferEnergy(creep);
        }
    }
 };

 function get_project(creep) {

    var structures = creep.room.find(FIND_STRUCTURES, {
        filter: function(object) {
            //console.log(object.structureType);
            if (object.hits < object.hitsMax/2 && (object.my || object.structureType == STRUCTURE_ROAD)) {
                return true;
            }
            return false;
        }
    });
    if (structures.length > 0) {
        Memory.repairtime = true;
    }


    structures = creep.room.find(FIND_STRUCTURES, {
        filter: function(object) {
            if (object.structureType != STRUCTURE_ROAD) {
                return false;
            }
            if (object.hits < object.hitsMax*.8) {
                return true;
            }
            return false;
        }
    });

    if (structures.length == 0) {
        Memory.repairtime = false;
    }

    var repairs = null;
    if (Memory.repairtime) {
        repairs = creep.pos.findClosest(FIND_STRUCTURES, {
            filter: function(object) {
                if (object.hits >= object.hitsMax*.8 || object.hits > 10000) {
                    return false;
                }
                if (object.structureType != STRUCTURE_ROAD && object.structureType != STRUCTURE_RAMPART) {
                    return false;
                }
                if (Memory.structures[object.id] == null || Memory.structures[object.id].builder == null || Game.getObjectById(Memory.structures[object.id].builder == null)) {
                    return true;
                }
                return false;
            }
        });
        if (repairs != null) {
            creep.memory.project = repairs.id;
            Memory.structures[repairs.id] = {};
            Memory.structures[repairs.id].builder = creep.id;
            return;
        }
    }


    var construction = creep.pos.findClosest(FIND_CONSTRUCTION_SITES, {
        filter: function(object) {
            if (Memory.structures[object.id] && Memory.structures[object.id].builder) {
                return false;
            }
            return true;
        }
    });
    
    if (construction != null) {
        creep.memory.project = construction.id;
        Memory.structures[construction.id] = {};
        Memory.structures[construction.id].builder = creep.id;
    } else {
        creep.memory.project = creep.room.controller.id;
    }

 }
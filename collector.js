/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */

//Remove targetId from CollectorTargets memory array.
 function  removeFromCollectorTargets(targetId) {
    var index = Memory.collectorTargets.indexOf(targetId);
    if (index > -1) Memory.collectorTargets.splice(index, 1);
 }

//Finds a builder which currently has no haulers serving it.
 function find_existing_builder(creep) {
    builder = creep.room.find(FIND_MY_CREEPS, {
        filter: function(object) {
            if (object.energy == object.energyCapacity) {
                object.memory.server == null;
                return false;
            }
            if (object.memory.role == "builder" && object.energy < object.energyCapacity && object.memory.server == null) {
                return true;
            }
        }
    });
    return builder;
 }

//Handles collector behaviour for using energy
 function returnEnergy(creep) {

    var extension = creep.pos.findClosest(FIND_MY_STRUCTURES,  {
        filter: function(object) {
            if (object.structureType == STRUCTURE_EXTENSION) {
                if (object.energy < object.energyCapacity) {
                    return true;
                }
            }
            return false;
        }
    });
    //find any builders that this hauler is currently serving
    var builder = creep.room.find(FIND_MY_CREEPS,  {
        filter: function(object) {
            if (object.memory.server == creep.id) {
                return true;
            }
        }
    });
    //find the closest spawn below 80% capacity
    var spawn = creep.pos.findClosest(FIND_MY_SPAWNS, {
        filter: function(object) {
            return object.energy < object.energyCapacity;
        }
    });
    //if the extensions are all full and the spawn is adequate, try and serve a builder
    if ((!extension && !spawn) || builder.length > 0) {
        if (builder.length == 0) {
            builder = find_existing_builder(creep);
        }
        if (builder.length > 0) {
            var myBuilder = builder[0];
             myBuilder.memory.server = creep.id;
            creep.moveTo(myBuilder);
            if (creep.transferEnergy(myBuilder) == 0) {
                myBuilder.memory.server = null;
            }
            return;
        }

    }
    //if there are no builders to server, try and fill closest extension.
    if (extension) {
        creep.moveTo(extension);
        creep.transferEnergy(extension);
        return;
    }
    //if there are no extensions, and no builders to serve, just try and fill spawn
    if (spawn && spawn.energy < spawn.energyCapacity) {
        creep.moveTo(spawn);
        creep.transferEnergy(spawn);
    }
    
 }

//handles creep behaviour for retrieving energy
function retrieveEnergy(creep, target) {
    creep.moveTo(target);
    creep.pickup(target);
}
 

 module.exports = {

    collect: function(creep) {

    //remove all targets from all dead creeps.

    //remove all targets from array if they are not in 
    //Game.spawns.Spawn1.room.find(Game.DROPPED_ENERGY))


        var memoryTargets = creep.room.find(FIND_DROPPED_ENERGY, {
            filter: function(object) {
                return object.id == creep.memory.target && object.energy > 0;
            }
        });
        
        //if energy has run out, remove from targets
        if (memoryTargets[0] === undefined || memoryTargets[0].energy === 0) {
            removeFromCollectorTargets(creep.memory.target);
            memoryTargets = [];
            creep.memory.target = null;
        }

        //if a target has been found in the creeps memory, retrieve that target.
        var targetEnergy;
        if (memoryTargets.length) {
            targetEnergy = memoryTargets[0];
            retrieveEnergy(creep, targetEnergy);
        }

        //if there 
        if(targetEnergy && creep.energy < creep.energyCapacity/2) {
            var closestEnemy = targetEnergy.pos.findClosest(FIND_HOSTILE_CREEPS);
                if (closestEnemy && closestEnemy.pos.inRangeTo(creep.pos, 4)) {
                    returnEnergy(creep);
                    return;
                }
        }

        //if the creep has no target
        if (creep.energy > creep.energyCapacity / 2 || targetEnergy === null) {
            if (targetEnergy) {
                creep.memory.target = null;
                removeFromCollectorTargets(targetEnergy.id);
            }
            returnEnergy(creep);
        }
    },

    //Finds all energy laying around, and assigns out chunks to all the haulers.
    assignTargets: function(room) {

        //create list of all targets of hauler creeps.
        Memory.collectorTargets = [];
        room.find(FIND_MY_CREEPS, {
            filter: function(object) {
                if (object.memory.role == "hauler" && object.memory.target) {
                    Memory.collectorTargets.push(object.memory.target);
                    return true;
                }
                return false;
            }
        });

        //find all haulers with targets that have expired, and remove their targets.
        var allHaulers = room.find(FIND_MY_CREEPS, {
            filter: function(object) {
                return (object.memory.role == "hauler" && object.memory.target);
            }
        });
        for (var hauler in allHaulers) {
            var a = Game.getObjectById(allHaulers[hauler].memory.target)
            if (a && a.pos === undefined) {
                allHaulers[hauler].memory.target = null;
            }
        }

        //create list of dropped energy starting with amount in descending order.
        var droppedEnergy = [];
        var numEnergies = room.find(FIND_DROPPED_ENERGY).length;
        for (var i = 0; i < numEnergies;i++) {
            var largest = 0;
            var largestId = null;
            room.find(FIND_DROPPED_ENERGY, {filter: makeEnergyFilter()});
            droppedEnergy[numEnergies-i] = largestId;
        }

        var newEnergyIds = [];
        var newEnergyArray = [];
        for (var eg in droppedEnergy) {
            var highEnergy = Game.getObjectById(droppedEnergy[eg]);
            if (highEnergy) {
                var energy = highEnergy.energy;
                if (energy > 2000) {
                    energy = 2000;
                }
                while (energy > 100) {
                    newEnergyArray[newEnergyArray.length] = energy;
                    newEnergyIds[newEnergyIds.length] = highEnergy.id;
                    energy = energy - 100;
                }
                if (energy > 0) {
                    newEnergyArray[newEnergyArray.length] = energy;
                    newEnergyIds[newEnergyIds.length] = highEnergy.id;
                }
            }
        }

        //find the closest available hauler for each bit of energy, starting with largest first.
        for (var i = 0;i < newEnergyArray.length;i++) {
            var energyIndex = newEnergyArray.indexOf(Math.max.apply(Math, newEnergyArray));
            var energy = newEnergyIds[energyIndex];
            newEnergyIds.splice(energyIndex, 1);
            newEnergyArray.splice(energyIndex, 1);
            var highEnergy = Game.getObjectById(energy);
            if (highEnergy) {
                var closestHauler = highEnergy.pos.findClosest(FIND_MY_CREEPS, {
                    filter: unusedHaulersFilter()
                });
                if (closestHauler) {
                    closestHauler.memory.target = highEnergy.id;
                    if (Memory.collectorTargets.indexOf(highEnergy.id) == -1) {
                        Memory.collectorTargets.push(highEnergy.id);
                    }
                }
            }
        }

        //list of filters
        function makeEnergyFilter() {
            return function(object) {
                //if (Memory.collectorTargets.indexOf(object.id) == -1 && droppedEnergy.indexOf(object.id) == -1) {
                    if (object.energy > largest) {
                        largest = object.energy;
                        largestId = object.id;
                    }
               // }
            };
        }

        function unusedHaulersFilter() {
            return function(object) {
                        return (object.memory.role == "hauler" && !object.memory.target && object.energy != object.energyCapacity);
            };
        }

    }
};



 /*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
 var fightMethods = require('fightMethods');
 module.exports = function(creep, spawn) {
 	if (creep.memory.action == null) creep.memory.action = "mine";
 	if (creep.energyCapacity > creep.energy && creep.room == spawn.room) {
		var closestSource = creep.pos.findClosest(FIND_MY_SPAWNS);
		creep.moveTo(closestSource);
		console.log(spawn.transferEnergy(creep));
		return;
	}
 	if (fightMethods.explore(creep)) {
	    var target = creep.room.controller;
	    if(!target.my) {
	    	if (creep.energyCapacity > creep.energy) {
    			var closestSource = target.pos.findClosest(FIND_SOURCES_ACTIVE);
    			creep.moveTo(closestSource);
    			creep.harvest(closestSource);
    		}
    		if (creep.energyCapacity == creep.energy) {
    			creep.moveTo(target);
    			creep.claimController(target);
    		}
	    } else {
	    	var spawnsite = creep.pos.findClosest(FIND_CONSTRUCTION_SITES);
	    	if (spawnsite) {
	    		if (creep.energyCapacity > creep.energy && creep.memory.action == "mine") {
	    			var closestSource = creep.pos.findClosest(FIND_SOURCES);
	    			console.log(closestSource);
	    			creep.moveTo(closestSource);
	    			console.log(creep.harvest(closestSource));
	    		}
	    		if (creep.energyCapacity == creep.energy || creep.memory.action == "build") {
	    			creep.memory.action = "build";
	    			creep.moveTo(spawnsite);
	    			creep.build(spawnsite);
	    		} if (creep.energy == 0) {
	    			creep.memory.action = "mine"
	    			var closestSource = spawnsite.pos.findClosest(FIND_SOURCES_ACTIVE);
	    			creep.moveTo(closestSource);
	    			creep.harvest(closestSource);
	    		}
	    	} else {
	    		var newSpawn = creep.pos.findClosest(FIND_MY_SPAWNS);
	    		if (creep.energyCapacity > creep.energy && creep.memory.action == "mine") {
	    			var closestSource = creep.pos.findClosest(FIND_SOURCES);
	    			console.log(closestSource);
	    			creep.moveTo(closestSource);
	    			console.log(creep.harvest(closestSource));
	    		}
	    		if (creep.energyCapacity == creep.energy || creep.memory.action == "build") {
	    			creep.memory.action = "build";
	    			creep.moveTo(newSpawn);
	    			console.log(creep.transferEnergy(newSpawn));
	    		} if (creep.energy == 0) {
	    			creep.memory.action = "mine"
	    			var closestSource = newSpawn.pos.findClosest(FIND_SOURCES_ACTIVE);
	    			creep.moveTo(closestSource);
	    			creep.harvest(closestSource);
	    		}
	    	}
	    }

    }
 };
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('spawner'); // -> 'a thing'
 */
 module.exports = function() {
    var fighters =  Game.spawns.Spawn1.room.find(Game.MY_CREEPS,  {
        filter: function(object) {
            return (object.getActiveBodyparts(ATTACK) > 0) || (object.getActiveBodyparts(RANGED_ATTACK));
        }
    });
    if (fighters.length > 12) {
        Memory.fighters = 12;
    } else {
        Memory.fighters = Math.max(fighters.length, 4);
	}
 };
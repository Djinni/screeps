/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
 module.exports = {

    explore: function(creep) {
        var dest = ['E7S6', 'E6S6'];

        for (var i = 0 ; i < dest.length;i++) {
            if (creep.room.name == dest[dest.length - 1]) {
                    return true;
            } else if (creep.room.name == dest[i]) {
                console.log(creep.room.findExitTo(dest[i+1]));
                creep.moveTo(creep.pos.findClosest(creep.room.findExitTo(dest[i+1])));
                return false;
            }
        }
    },

    rest: function(creep) {
        // var distance = Memory.fighters;
        // if (creep.getActiveBodyparts(HEAL)) {
        //     distance = distance - 1;
        // }
        // else if (creep.getActiveBodyparts(RANGED_ATTACK)) {
        //     distance = distance - 2;
        // }
// var target = creep.findClosest(FIND_MY_SPAWNS);
// if (target && creep.pos.findPathTo(target).length < 3) {
//     creep.moveTo(creep.pos.x + creep.pos.x - target.x, creep.pos.y + creep.pos.y - target.y );
// }


        // if (creep.pos.findPathTo(Game.spawns.Spawn1).length > distance) {
        //     var flags = Game.spawns.Spawn1.room.find(Game.FLAGS);
        //     var lastChar = creep.id.substr(creep.id.length - 1);
        //     var flag = flags[lastChar % flags.length];
        //     creep.moveTo(flag);
        // //}
    },

    rangedAttack: function(creep) {
        var target = creep.pos.findClosest(FIND_HOSTILE_CREEPS);
        if(target) {
            if (target.pos.inRangeTo(creep.pos, 3) ) {
                creep.rangedAttack(target);
                return target;
            }
        }
        return null;
    },

    meleeAttack: function(creep) {
    if (creep.getActiveBodyparts(ATTACK) === 0) return false;

        var target = creep.pos.findClosest(Game.HOSTILE_CREEPS);
        if(target && target.pos.inRangeTo(Game.spawns.Spawn1.pos, Memory.fighters * 5) ) {
            creep.moveTo(target);
            creep.attack(target);
            return true;
        }
        return false;
    },

    findHealer: function(creep) {
        var target = creep.pos.findClosest(FIND_MY_CREEPS, {
            filter: function(object) {
                return object.getActiveBodyparts(HEAL) > 0;
            }
        });
        if (creep.getActiveBodyparts(ATTACK) === 0 && target && target.pos.inRangeTo(Game.spawns.Spawn1.pos, 2) === null) {
            creep.moveTo(target);
            return true;
        }
        return false;
    },

    healFriends: function(creep) {
    if (creep.getActiveBodyparts(HEAL) === 0) return false;

    var civilians = ['worker', 'builder', 'collector'];
    var militaryTarget = creep.pos.findClosest(FIND_MY_CREEPS, {
        filter: function(object) {
            return object.hitsMax != object.hits && object != creep && civilians.indexOf(object.memory.role) == -1;
        }
    });
    var civTarget = creep.pos.findClosest(FIND_MY_CREEPS, {
        filter: function(object) {
            return object.hitsMax != object.hits && object != creep && civilians.indexOf(object.memory.role) > -1;
        }
    });
    var target = null;
    if (civTarget) target = civTarget;
    if (militaryTarget) target = militaryTarget;

    

    if (target) {
        var closestEnemies = target.pos.findInRange(FIND_HOSTILE_CREEPS, 1);
        var currentThreat = creep.pos.findClosest(FIND_HOSTILE_CREEPS);
        if (currentThreat && creep.pos.inRangeTo(currentThreat.pos, 1)) {
            var shortestRange = 100;
            var coords = null;
            for (var x = -1; x<2;x++) {
                for (var y = -1;y<2;y++) {
                    if (x !== 0 && y !== 0) {
                        var xdist = target.pos.x + x;
                        var ydist = target.pos.y + y;
                        if (!(target.pos.findInRange(FIND_HOSTILE_CREEPS, 1).length)) {
                            var range = creep.room.getPositionAt(x, y);
                            if (range < shortestRange){
                                shortestRange = range;
                                coords = creep.room.getPositionAt(x, y);
                            }
                        }
                    }
                }
            }
            creep.moveTo(coords);
            creep.heal(target);
            //for each position around target
            //try and move away from the enemy, while staying next to target.
        } else {
            creep.moveTo(target);
            creep.heal(target);
        }
        return true;
    }
    return false;
    },

    followUnitType: function(creep, type) {
    var target = creep.pos.findClosest(FIND_MY_CREEPS, {
            filter: function(object) {
                return object.getActiveBodyparts(type) > 0 && object != creep;
            }
        });
        if(target) {
            creep.moveTo(target);
            return true;
        }
     return false;
    },

    kite: function(creep, target) {
        if (target.pos.inRangeTo(creep.pos, 2)) {
            creep.moveTo(creep.pos.x + creep.pos.x - target.pos.x, creep.pos.y + creep.pos.y - target.pos.y );
            return true;
        } else if (target.pos.inRangeTo(creep.pos, 3)) {
            return true;
        }
        return false;
    }
};

/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
module.exports = function(spawn) {

	if (Memory.seenCreeps === undefined) Memory.seenCreeps = [];

	var closeCreeps = spawn.room.find(FIND_HOSTILE_CREEPS, {
        filter: function(object) {
            return object.pos.inRangeTo(spawn.pos, 11) && Memory.seenCreeps.indexOf(object.id) == -1;
        }
    });
    for (var newCreep in closeCreeps) {
    	Memory.seenCreeps.push(closeCreeps[newCreep].id);
    	if (closeCreeps[newCreep].pos.findInRange(Game.FLAGS, 5).length == 0) {
    		spawn.room.createFlag(closeCreeps[newCreep].pos);
            //we should return after this, so we dont accidentally make lots of flags near each other.
            return;
    	}
    }

};
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
var fightMethods = require('fightMethods');
module.exports = function(creep) {

    if (fightMethods.findHealer(creep)) return;
    if (fightMethods.meleeAttack(creep)) return;

    fightMethods.rest(creep);
};
module.exports = function (creep) {
//console.log( creep.room.find(FIND_MY_SPAWNS)[0]])
//console.log( Memory.sourceArray[creep.room.find(FIND_MY_SPAWNS)[0]])
    if(creep.memory.sourceId === undefined &&  Memory.sourceArray[creep.pos.findClosest(FIND_MY_SPAWNS).name]) 
    {
        var availableSources = Memory.sourceArray[creep.pos.findClosest(FIND_MY_SPAWNS).name].filter(function (source) {
            return !source [1];
        });
        console.log('c' + availableSources)
        if(availableSources.length) {
            var source = availableSources[0];
            creep.memory.sourceId = source[0];
            if(!source[1]) {
                source[1] = creep.id;
            }
            else {
                source[2] = creep.id;
            }

        }
    }
    //if there is no target on this harvester.
    if (creep.memory.sourceId) {
        var source = Game.getObjectById(creep.memory.sourceId);
        if(source) {
            creep.moveTo(source);
            creep.harvest(source);
        }
    }
};
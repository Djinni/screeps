/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
var fightMethods = require('fightMethods');
 module.exports = function(creep) {
 	if (fightMethods.explore(creep)) {
	    var foundHeal = fightMethods.healFriends(creep);
	    
	    if (!foundHeal) {
	        if (fightMethods.followUnitType(creep, RANGED_ATTACK) === false) creep.move(BOTTOM)//fightMethods.rest(creep);
	    }
	}
 };
 /*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
 var fightMethods = require('fightMethods');
 module.exports = function(creep) {
 	if (fightMethods.explore(creep)) {
	    var target = fightMethods.rangedAttack(creep);
	    console.log('aAa');
	    if(target === null) {
	        target = creep.pos.findClosest(FIND_HOSTILE_CREEPS);
	        if (target) {
	            creep.moveTo(target);
	            return;
	        }
	    } else if (fightMethods.kite(creep, target)) return;

    }
 };
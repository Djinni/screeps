

var harvester = require('harvester');
var builder = require('builder');
var guard = require('guard');
var healer = require("healer");
var paladin = require('paladin');
var collect = require('collector');
var kiter = require("kiter");
var spawner = require('spawner');
var kiter = require('kiter');
var paladin = require('paladin');
var counter = require('counter');
var flagger = require('flagger');
var colony = require('colony');




//return;
var allRooms = [];
for (var spawn in Game.spawns) {
    //flagger checks for danger around all spawns
    flagger(Game.spawns[spawn]);

    if (allRooms.indexOf(Game.spawns[spawn].room) == -1) {
        allRooms[allRooms.length-1] = Game.spawns[spawn].room;
        collect.assignTargets(Game.spawns[spawn].room);
    }
}


if (Memory.structures === undefined) {
    Memory.structures = {};
} else {
    //remove any structures with dead references in memory.
    for (var eg in Memory.structures) {
        if (Memory.structures[eg] != null) {
            var strucBuilder = Game.getObjectById(eg);
            if (strucBuilder == null) {
                Memory.structures[eg] = null;
            }       
        }
    }
}
if (Memory.sourceArray === undefined) {
    Memory.sourceArray = {};
}
var spawnList = [Game.spawns.Spawn1, Game.spawns.Spawn2];
for (var spawn in spawnList) {
    if (!Memory.sourceArray[spawnList[spawn].name] && spawnList[spawn]) {

        var sourceArray = spawnList[spawn].room.find(FIND_SOURCES);
        var orderedSourceArray = [];

        sourceArray.forEach(function (source) {
            var stepsHome = source.pos.findPathTo(spawnList[spawn], {ignoreCreeps : true}).length;
            orderedSourceArray.push([source.id, null, null, stepsHome]);
        });

        orderedSourceArray.sort( function (a, b) {
            return a[3] > b[3];
        });

        Memory.sourceArray[spawnList[spawn].name] = orderedSourceArray;
    }
}

spawner();

counter();

for(var name in Game.creeps) {
    if (Game.getUsedCpu() > 100) {
        console.log('exiting early')
        return;
    }
    var creep = Game.creeps[name];

    if(creep.memory.role == 'worker') {
        harvester(creep);
    }

    if(creep.memory.role == 'builder') {
        builder(creep);
    }

    if (creep.memory.role == 'guard' || creep.memory.role == 'fighter') {
        guard(creep);
    }

    if (creep.memory.role == 'colony') {
        colony(creep, Game.spawns.Spawn1);
    }
    
    if (creep.memory.role == 'kiter' || creep.memory.role == 'tower' || creep.memory.role == 'supertower') {
        kiter(creep);
    }

    if (creep.memory.role == 'healer') {
        healer(creep);
    }

    if (creep.memory.role == 'paladin') {
        paladin(creep);
    }

    if (creep.memory.role == 'collector' || creep.memory.role == 'hauler') {
        collect.collect(creep);
    }
}
console.log(Game.getUsedCpu());


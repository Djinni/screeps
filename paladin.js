/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
var fightMethods = require('fightMethods');
module.exports = function(creep) {
    var target = fightMethods.rangedAttack(creep);
    var foundHeal = fightMethods.healFriends(creep);
    
    if (!foundHeal) {
        if(target === null) {
            target = creep.pos.findClosest(FIND_HOSTILE_CREEPS);
            if (target && target.pos.inRangeTo(Game.spawns.Spawn1.pos, Memory.fighters * 5)) {
                creep.moveTo(target);
                return;
            }
        } else if (fightMethods.kite(creep, target)) return;
        
        fightMethods.rest(creep);
    }
};